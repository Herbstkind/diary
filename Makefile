EXE=/usr/bin/diary
.PHONY: $(EXE) clean install uninstall

install: $(EXE)
	sudo install -m 0755 ./diary.sh $(EXE)

uninstall:
	sudo rm $(EXE)

clean:
	rm -f *~
	rm -f \#*
