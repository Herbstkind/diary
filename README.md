## A simple diary script for plural systems on Discord

This script enables you to post diary entries to a Discord channel that has hooks.

### Installation

Either run the script from its directory (`./diary.sh`), or run `make install` and then just invoke `diary` (`/usr/bin/` must be in the $PATH).
To uninstall just run `make uninstall`.

### Usage

You define your system members and their hooks in `~/.config/diary/system`, e.&nbsp;g.:
```
Alice https://discordapp.com/api/webhooks/<webhook of Alice>
Bob   https://discordapp.com/api/webhooks/<webhook of Bob>
Eve   https://discordapp.com/api/webhooks/<webhook of Eve>
```
Theoretically there is no limit on the characters you can use in the member names, as long as there is at least one whitespace character in front of the webhook URL. In practice you can use all characters that don't have to be escaped in grep-style regular expressions enclosed by "". If you're not sure what that means, just stick to alphanumeric characters and whitespace :)

Call diary like this: `diary Alice`. Diary will invoke the expanded value of `$VISUAL`, `$EDITOR`, `$(which vim)` or `$(which vi)`, depending on which first yields a non-empty string. Write your entry, save it, and exit. Diary will post the entry to (in this example) Alice's webhook and also store it in `~/.config/diary/<entryname>`, where `<entryname>` is the current time in the form `YYYY-MM-DD-hh-mm-ss`.

#### Not storing diary entry to disk

If you don't want the entry to be stored on local disk, but only pushed to Discord, invoke diary with the `-n` or `--no-store` switch: `diary --no-store Alice`. The member name has to be the last argument.

#### Choosing an author at random

You can also let diary let choose a system member for you at random. Just supply the `-r` or `--random-author` parameter. It doesn't matter whether it's supplied as first or second parameter (after `-n`). If you supply a member name together with `-r` the member name will be ignored.

#### Drafting

If you don't want your entry to be posted just yet (or if your entry is too long and you don't want to reedit it), diary will save it as a draft and next time use it as a template.
