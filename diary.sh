#!/usr/bin/bash

DIARYCONFDIR=~/".config/diary" # Don't quote the tilde, else it won't be expanded
SYSTEMFILE="$DIARYCONFDIR/system"
DRAFTFILE="$DIARYCONFDIR/draft"
HOOKPREFIX="https://discordapp.com/api/webhooks/"

function error {
	(>&2 echo "Error: $1")
	exit 1
}

function warning {
	(>&2 echo "Warning: $1")
}

function catlinetoline {
	filename="$1"
	linefrom="$2"
	lineto="$3"
	head -"$lineto" "$filename" | tail -"$((lineto - linefrom + 1))"
}


# Check dependencies
command -v curl > /dev/null || error "No curl executable found"

# Check config directory
if [ ! -d "$DIARYCONFDIR" ]; then
	if [ -e "$DIARYCONFDIR" ]; then
		error "$DIARYCONFDIR exists, but is not a directory"
		exit 1
	fi
	mkdir "$DIARYCONFDIR" || exit 1
fi

# Check arguments
nostore=""
randomauthor=""
for _ in {1..2}; do
	if [ "$1" = "--no-store" ] || [ "$1" = "-n" ]; then
		nostore=1
		shift
	fi
	if [ "$1" = "--random-author" ] || [ "$1" = "-r" ]; then
		randomauthor=1
		shift
	fi
done
author="$1"

# choose author at random
if [ -n "$randomauthor" ]; then
	# count names
	count=$(grep -c "^.\+\s\+$HOOKPREFIX" "$SYSTEMFILE")

	# choose name at random
	authorindex=$((RANDOM % count))
	index=0
	while read -r name; do
		if [ "$authorindex" -eq "$index" ]; then
			author="$name"
			break
		fi
		((index++))
	done < <(grep "^.\+\s\+$HOOKPREFIX" "$SYSTEMFILE" | grep -o "^.\+\s" | grep -o "^.\+\S")
	echo "System member '$author' will write the new diary entry. Press enter to continue (or Control-C to abort)."
	read -r
fi

# Check author
if [ -z "$author" ]; then
	error "Mandatory argument missing: Name of system member to post diary entry as."
fi

# Check author in system file
touch "$SYSTEMFILE" > /dev/null || exit 1 # errors of touch are descriptive enough
grep -qi "^$author\s\+$HOOKPREFIX" "$SYSTEMFILE" || error "Member $author does not seem to be in the system file"

# Extract hook for author
hook=$(grep -i "^$author\s\+$HOOKPREFIX" "$SYSTEMFILE" | grep -o "$HOOKPREFIX.\+")

# Choose editor
editprgm="$VISUAL"
if [ -z "$editprgm" ]; then
	editprgm="$EDITOR"
	if [ -z "$editprgm" ]; then
		editprgm=$(command -v vim)
		if [ -z "$editprgm" ]; then
			editprgm=$(command -v vi)
			if [ -z "$editprgm" ]; then
				error "No editor found."
			fi
		fi
	fi
fi

# Create diary entry file from draft
if [ -n "$nostore" ]; then
	filename=$(mktemp)
else
	filename="$DIARYCONFDIR/$(date +%F-%H-%M-%S)"
fi
touch "$DRAFTFILE" || exit 1

# Edit file
cp "$DRAFTFILE" "$filename" || error "Cannot make diary entry file from draft"
"$editprgm" "$filename" || error "Your editor exited with an error. Aborting diary."

# Check if file contains anything
length=$(wc -c "$filename" | cut -d' ' -f1)
if [ "$length" -eq 0 ]; then
	rm "$filename"
	error "The diary entry is empty. Aborting."
fi

# Check file length
filesize=$(wc -c "$filename" | cut -d' ' -f1)
if [ "$filesize" -ge 2000 ]; then
	warning "File too large to post all at once. File will be splitted into several posts."
fi

# Prompt for posting
authorcasecorrected=$(grep -oi "^$author" "$SYSTEMFILE")
echo "Are you sure you want to post this diary entry, $authorcasecorrected?"
echo "Press ENTER to post the entry or CONTROL-C to abort (entry will be saved as draft for next time)."
trap 'mv "$filename" "$DRAFTFILE"; exit' SIGINT; read -r

# Split file into chunks, if necessary, each <=2000 bytes
linefrom=1
maxlineto=$(wc -l "$filename" | cut -d' ' -f1)
chunkindex=0
while true; do

	# Smallest possible chunk (contains only one line)
	lineto="$linefrom"
	chunk=$(catlinetoline "$filename" "$linefrom" "$lineto")
	chunksize=$(echo -n "$chunk" | wc -c)

	# Figure out maximum chunk size
	while [ "$chunksize" -le 2000 ]; do

		# Check next bigger chunk
		lineto_=$((lineto + 1))
		chunk_=$(catlinetoline "$filename" "$linefrom" "$lineto_")
		chunksize_=$(echo -n "$chunk_" | wc -c)
		if [ "$chunksize_" -gt 2000 ]; then
			break
		fi

		# New chunk still fits, so make it the current chunk
		chunk="$chunk_"
		lineto="$lineto_"
		chunksize="$chunksize_"

		# If we reached the end of the file, stop probing.
		if [ "$lineto" -ge "$maxlineto" ]; then
			break
		fi
	done

	# Check chunk size
	if [ "$chunksize" -gt 2000 ]; then
		error "At least one chunk is too big to be posted (>2000 bytes)"
	fi

	# Store chunk for posting
	chunks[$chunkindex]="$chunk"
	((chunkindex++))

	# Stop collecting chunks if end of file reached
	if [ "$lineto" -ge "$maxlineto" ]; then
		break
	fi

	# Prepare for next chunk
	linefrom=$((lineto + 1))
done

# Post chunks
echo "Posting diary entry..."
chunkamount=${#chunks[@]}
for chunk in "${chunks[@]}"; do

	# Post chunk
	response=$(curl -F "content=$chunk" "$hook")

	# Check server response
	if [ -n "$response" ]; then
		mv "$filename" "$DRAFTFILE"
		error "Could not post diary entry. Saved entry as draft."
	fi
done
